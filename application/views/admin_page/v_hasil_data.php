<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables/dataTables.min.css">
<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>assets/DataTables/dataTables.min.js"></script>

<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			<div class="form-title">
				<h3>Daftar Hasil Data Mahasiswa</h3>
				<div class="clearfix"></div>
			</div>
			
			<div class="form-body">
				<div>
					<button id="icondownload" type="button" class="btn btn-primary"><i class="fa fa-download"></i> Unduh Hasil Data Mahasiswa</button>
					<div class="clearfix"></div>
				</div>
				<br />
				<table id ="myTable" class="table table-striped table-bordered">
					<thead>
						<tr>			
							<th>No.</th>
							<th>NIM</th>
							<th>Nama</th>
							<th>Jurusan</th>
							<th>Tanggal Konseling</th>
							<th>Tanggal Verifikasi</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 1;
						foreach ($list_result as $list) {
						?>
						<tr>				
							<td><?php echo $i."."; ?></td>
							<td><?php echo $list['nim']; ?></td>
							<td><?php echo $list['nama']; ?></td>
							<td><?php echo $list['jurusan']; ?></td>
							<td><?php echo date('d-M-Y H:i:s', strtotime($list['created_date'])); ?></td>
							<td><?php echo date('d-M-Y H:i:s', strtotime($list['date_validasi'])); ?></td>
							<td>
								<a href="<?php echo base_url() ?>mahasiswa/view_result/<?php echo $list['nim'] ?>" title="Lihat <?php echo $list['nim']; ?> "> <i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;
								<a href="<?php echo base_url() ?>mahasiswa/hapus_result/<?php echo $list['nim'] ?>" title="Hapus <?php echo $list['nim']; ?> "> <i class="fa fa-trash"></i></a>&nbsp;&nbsp;&nbsp;
                                <a href="<?php echo base_url() ?>mahasiswa/cetak_pdf/<?php echo $list['nim'] ?>" title="Cetak PDF <?php echo $list['nim']; ?> "> <i class="fa fa-print"></i></a>&nbsp;&nbsp;&nbsp;
							</td>
						</tr>
						<?php $i++; ?>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready( function () {
		$('#myTable').DataTable();
	} );
	
	icondownload.onclick = function() {
		window.open('<?php echo base_url(); ?>mahasiswa/download');
	};
</script>