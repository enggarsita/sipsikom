<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables/dataTables.min.css">
<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>assets/DataTables/dataTables.min.js"></script>

<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			<div class="form-title">
				<h4>Lihat Kamus PPDGJ III & DSM 5</h4>
				<div class="clearfix"></div>
			</div>
			<div class="panel-info widget-shadow">
				<!-- START DATA CONTENT -->
				<div class="col-md-12 panel-grids">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title"><?php echo $dt_view[0]->nm_kamus; ?></h3>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body"><?php echo $dt_view[0]->isi_kamus; ?></div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- END DATA CONTENT -->
				<div align="center">
					<button id="iconback" type="button" class="btn btn-primary"><i class="fa fa-back"></i> Kembali</button>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<script type="text/javascript">
	iconback.onclick = function() {
		window.location.replace('<?php echo base_url(); ?>kamus');
	};
</script>