<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			<div class="form-title">
				<h4>Tambah Kamus PPDGJ III & DSM 5</h4>
				<div class="clearfix"></div>
			</div>
			<div class="form-body">
				<!-- START DATA CONTENT -->
				<form class="form-horizontal" action="<?php echo base_url() ?>kamus/save_add" method="post">
					<div class="form-group">
						<label class="col-sm-2 control-label">Judul</label>
						<div class="col-sm-9">
							<input type="text" name="judul" id="judul" class="form-control" required />
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Deskripsi</label>
						<div class="col-sm-9">
							<textarea class="ckeditor" name="isi"></textarea>
							<div class="clearfix"> </div>
						</div>
					</div>
					
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-9">
							<button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> Simpan</button>
							<div class="clearfix"> </div>
						</div>
					</div>					
				</form>
				<!-- END DATA CONTENT -->
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
