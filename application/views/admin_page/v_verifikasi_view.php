<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables/dataTables.min.css">
<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>assets/DataTables/dataTables.min.js"></script>

<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			<div class="form-title">
				<h4>Lihat Detail Verifikasi</h4>
				<div class="clearfix"></div>
			</div>
			<div class="panel-info widget-shadow">
				<!-- START DATA CONTENT -->
				<div class="col-md-12 panel-grids">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">
								Form Biodata Mahasiswa
								<br />
							</h3>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							<div class="form-group">
								<label class="col-sm-2 control-label">NIM</label>
								<div class="col-sm-9">
									<input type="text" name="nim" id="nim" class="form-control" maxlength="15" value="<?php echo $data->nim; ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama Lengkap</label>
								<div class="col-sm-9">
									<input type="text" name="nama" id="nama" class="form-control" value="<?php echo $data->nama; ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Alamat</label>
								<div class="col-sm-9">
									<input type="text" name="alamat" id="alamat" class="form-control" value="<?php echo $data->alamat; ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Kota / Kabupaten</label>
								<div class="col-sm-9">
									<input type="text" name="kabupaten" id="kabupaten" class="form-control" value="<?php echo $kabupaten; ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Provinsi</label>
								<div class="col-sm-9">
									<input type="text" name="provinsi" id="provinsi" class="form-control" value="<?php echo $provinsi; ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jurusan</label>
								<div class="col-sm-9">
									<input type="text" name="jurusan" id="jurusan" class="form-control" value="<?php echo $data->jurusan; ?>" readonly />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Hasil</label>
								<div class="col-sm-9">
									<input type="text" name="hasil" id="hasil" class="form-control" value="<?php echo $data_solusi[0]->jenis; ?>" readonly />
								</div>
							</div>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">
								Form Keluhan Tanggal : <?php echo date('d-M-Y H:i:s', strtotime($dt_esay[0]->created_date)); ?>
								<br />
							</h3>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							<?php echo $dt_esay[0]->keterangan; ?>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">
								Form Pertanyaan Tanggal : <?php echo date('d-M-Y H:i:s', strtotime($dt_view[0]->created_date)); ?>
								<br />
							</h3>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							
							<?php
							$i = 1;
							foreach ($dt_view as $list) {
							?>
							<p>
								<?php echo $i.". ".$list->pertanyaan; ?>
								( <strong><?php if($list->jawaban==0){ echo "Tidak"; } else { echo "Ya"; } ?></strong> )
							</p>
							<?php $i++; ?>
							<?php } ?>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- END DATA CONTENT -->
				<div align="center">
					<button id="iconback" type="button" class="btn btn-primary"><i class="fa fa-back"></i> Kembali</button>
					<div class="clearfix"></div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<script type="text/javascript">
	iconback.onclick = function() {
		window.location.replace('<?php echo base_url(); ?>mahasiswa/verifikasi');
	};
</script>