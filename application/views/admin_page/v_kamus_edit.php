<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables/dataTables.min.css">
<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>assets/DataTables/dataTables.min.js"></script>

<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			<div class="form-title">
				<h4>Edit Kamus PPDGJ III & DSM 5</h4>
				<div class="clearfix"></div>
			</div>
			<div class="form-body">
				<!-- START DATA CONTENT -->
				<form class="form-horizontal" action="<?php echo base_url() ?>kamus/save_edit" method="post">
					<div class="form-group">
						<label class="col-sm-2 control-label">Judul</label>
						<div class="col-sm-9">
							<input type="text" name="judul" id="judul" class="form-control" value="<?php echo $dt_edit[0]->nm_kamus; ?>" required />
							<div class="clearfix"> </div>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Deskripsi</label>
						<div class="col-sm-9">
							<textarea class="ckeditor" name="isi"><?php echo $dt_edit[0]->isi_kamus; ?></textarea>
							<div class="clearfix"> </div>
						</div>
					</div>
					
					<input type="hidden" name="id" id="id" class="form-control" value="<?php echo $dt_edit[0]->id; ?>" required />
					
					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-9">
							<button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> Simpan</button>
							<div class="clearfix"> </div>
						</div>
					</div>
				</form>
				<!-- END DATA CONTENT -->
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
