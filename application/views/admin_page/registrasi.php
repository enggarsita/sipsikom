<!DOCTYPE HTML>
<html>
<head>
<title>SIPSI.KOM</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='<?php echo base_url(); ?>assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts-->
 
<!-- Metis Menu -->
<script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

</head>

<body >
<div class="main-content">
	
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h2 class="title1">Registrasi Pembimbing Konseling<br />SIPSI.KOM</h2>
				<div class="sign-up-row widget-shadow">
					<form action="<?php echo base_url() ?>registrasi/cek" method="post">
						<div class="sign-u">
							<input type="text" name="nama" class="user" placeholder="Nama Lengkap" required />
							<div class="clearfix"> </div>
						</div>
						<div class="sign-u">
							<input type="text" name="nohp" class="phone" placeholder="Nomor Handphone" required />
							<div class="clearfix"> </div>
						</div>
						<div class="sign-u">
							<input type="text" name="username" class="user" placeholder="Username" required />
							<div class="clearfix"> </div>
						</div>
						<div class="sign-u">
							<input type="password" id="password-field" name="password" class="lock" placeholder="Kata Sandi" required />
							<input type="checkbox" id="form-checkbox"> 
							Tampilkan Sandi
							<div class="clearfix"> </div>
						</div>
						
						<div align="center">
							<button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> Registrasi</button>
							<div class="clearfix"> </div>
						</div>
						<div class="registration">
							Sudah Registrasi Akun ?<a class="" href="login"> Masuk</a>
							<div class="clearfix"> </div>
						</div>
					</form>
				</div>
			</div>
			
		</div>
		<!--footer-->
		<div class="footer">
		   <p>&copy; 2019 <a href="<?php echo base_url(); ?>">SIPSI.KOM</a></p>
		</div>
        <!--//footer-->
	</div>
	
	<!--scrolling js-->
	<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"> </script>
	<!-- //Bootstrap Core JavaScript -->
   
</body>
</html>

<script>
$(document).ready(function(){		
		$('#form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('#password-field').attr('type','text');
			}else{
				$('#password-field').attr('type','password');
			}
		});
	});
</script>