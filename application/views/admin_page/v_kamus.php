<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables/dataTables.min.css">
<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>assets/DataTables/dataTables.min.js"></script>

<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			<div class="form-title">
				<h4>Kamus PPDGJ III & DSM 5</h4>
				<div class="clearfix"></div>
			</div>
			<div class="form-body">
				<div>
					<button id="iconadd" type="button" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah Kamus PPDGJ III & DSM 5</button>
					<div class="clearfix"></div>
				</div>
				<br />
			
				<!-- START DATA TAMBAHAN DIRI PEGAWAI -->
				<table id ="myTable" class="table table-striped table-bordered">
					<thead>
						<tr>			
							<th>No.</th>
							<th>Judul</th>
							<th>Aksi</th>
						</tr>
					</thead>
					<tbody>
						<?php
						$i = 1;
						foreach ($list_kamus as $list) {
						?>
						<tr>				
							<td><?php echo $i."."; ?></td>
							<td><?php echo $list['nm_kamus']; ?></td>
							<td>
								<a href="<?php echo base_url() ?>kamus/view/<?php echo $list['id'] ?>" title="Lihat <?php echo $list['nm_kamus']; ?> "> <i class="fa fa-eye"></i></a>&nbsp;&nbsp;&nbsp;
								<a href="<?php echo base_url() ?>kamus/edit/<?php echo $list['id'] ?>" title="Ubah <?php echo $list['nm_kamus']; ?> "> <i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;
								<a href="<?php echo base_url() ?>kamus/hapus/<?php echo $list['id'] ?>" title="Hapus <?php echo $list['nm_kamus']; ?> "> <i class="fa fa-trash"></i></a>&nbsp;&nbsp;&nbsp;
							</td>
						</tr>
						<?php $i++; ?>
						<?php } ?>
					</tbody>
				</table>
				<!-- END DATA DIRI PEGAWAI -->
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<script type="text/javascript">
	$(document).ready( function () {
		$('#myTable').DataTable();
	} );
	
	iconadd.onclick = function() {
		window.location.replace('<?php echo base_url(); ?>kamus/add');
	};
</script>