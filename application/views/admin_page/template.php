<!DOCTYPE HTML>
<html>
<head>
<title>SIPSI.KOM</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='<?php echo base_url(); ?>assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="<?php echo base_url(); ?>assets/js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

<!-- start datepicker -->
<script src="<?php echo base_url(); ?>assets/datepicker/js/bootstrap-datepicker.js"></script>
<link href="<?php echo base_url(); ?>assets/datepicker/css/bootstrap-datepicker.css" rel="stylesheet">
<!-- end datepicker -->

<!-- start ckeditor -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<!-- end ckeditor -->

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"> </script>
<!-- //Bootstrap Core JavaScript -->

<!-- requried-jsfiles-for owl -->
<link href="<?php echo base_url(); ?>assets/css/owl.carousel.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.js"></script>
<script>
	$(document).ready(function() {
		$("#owl-demo").owlCarousel({
			items : 3,
			lazyLoad : true,
			autoPlay : true,
			pagination : true,
			nav:true,
		});
	});
</script>
<!-- //requried-jsfiles-for owl -->
</head> 

<body class="cbp-spmenu-push">
	<div class="main-content">
		<!--left-fixed -navigation-->
		<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
			<aside class="sidebar-left">
				<nav class="navbar navbar-inverse">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<h1><a class="navbar-brand" href="<?php echo base_url(); ?>"><span class="fa fa-medkit"></span> SIPSI.KOM</a></h1>
					</div>
					
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="sidebar-menu">
							<li class="header">MAIN NAVIGATION</li>
							<?php
							foreach ($menus as $dt_menu){
								if ($dt_menu->sub_menu=='0'){
									echo "
										<li class='treeview'>
											<a href='".base_url()."".$dt_menu->link_menu."'><i class='".$dt_menu->class_menu."'></i> <span>".strtoupper($dt_menu->nm_menu)."</span></a>
										</li>
										";
								} else {
									$id_menu = $dt_menu->id_menu;
									$submenu = $this->M_Menu->get_submenu($dt_menu->id_menu);
									
									echo "
										<li class='treeview'>
											<a href='#'><i class='".$dt_menu->class_menu."'></i> <span>".strtoupper($dt_menu->nm_menu)."</span><i class='fa fa-angle-left pull-right'></i></a>
											<ul class='treeview-menu'>
										";
										foreach ($submenu as $dt_submenu){
									echo "
												<li><a href='".base_url()."".$dt_submenu->link_menu."'>&emsp;<i class='".$dt_submenu->class_menu."'></i> <span>".strtoupper($dt_submenu->nm_menu)."<span></a></li>
										";
										}
									echo "
											</ul>
										</li>
										";
								}
							}
							?>
						</ul>
					</div>
					<div class="clearfix"> </div>
					<!-- /.navbar-collapse -->
				</nav>
			</aside>
		</div>
		<!--left-fixed -navigation-->
		
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				<!--toggle button start-->
				<button id="showLeftPush"><i class="fa fa-bars"></i></button>
				<!--toggle button end-->
				<div class="clearfix"> </div>
			</div>
			<div class="header-right">
				<div class="profile_details" style="padding: 15px;">
					<button id="iconlogout" type="button" class="btn btn-primary"><i class="fa fa-sign-out"></i> Keluar</button>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
		</div>
		<!-- //header-ends -->
		
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<?php $this->load->view($container); ?>
			</div>
		</div>
		<!-- //main content end-->
		
	<!--footer-->
	<?php $this->load->view('slice/v_footer'); ?>
    <!--//footer-->
	</div>
	
</body>
</html>

<script>
	iconlogout.onclick = function() {
		var r = confirm("Apakah Anda yakin ingin keluar?");
		if (r) {
			window.location.replace('<?php echo base_url(); ?>Login/logout');
		}
	};
</script>

<!-- new added graphs chart js-->
<script src="<?php echo base_url(); ?>assets/js/Chart.bundle.js"></script>
<script src="<?php echo base_url(); ?>assets/js/utils.js"></script>

<!-- Classie --><!-- for toggle left push menu script -->
<script src="<?php echo base_url(); ?>assets/js/classie.js"></script>
<script>
	var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
	showLeftPush = document.getElementById( 'showLeftPush' ),
	body = document.body;

	showLeftPush.onclick = function() {
		classie.toggle( this, 'active' );
		classie.toggle( body, 'cbp-spmenu-push-toright' );
		classie.toggle( menuLeft, 'cbp-spmenu-open' );
		disableOther( 'showLeftPush' );
	};


	function disableOther( button ) {
		if( button !== 'showLeftPush' ) {
			classie.toggle( showLeftPush, 'disabled' );
		}
	}
</script>
<!-- //Classie --><!-- //for toggle left push menu script -->

<!--scrolling js-->
<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
<!--//scrolling js-->

<!-- side nav js -->
<script src='<?php echo base_url(); ?>assets/js/SidebarNav.min.js' type='text/javascript'></script>
<script>
	$('.sidebar-menu').SidebarNav()
</script>
<!-- //side nav js -->

<!-- for index page weekly sales java script -->
<script src="<?php echo base_url(); ?>assets/js/SimpleChart.js"></script>
<!-- //for index page weekly sales java script -->

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"> </script>
<!-- //Bootstrap Core JavaScript -->