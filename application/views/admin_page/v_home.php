<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/DataTables/dataTables.min.css">
<script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>assets/DataTables/dataTables.min.js"></script>

<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			<div class="form-title">
				<h4>Dashboard</h4>
				<div class="clearfix"></div>
			</div>
			<div class="main-page">
				<div class="col_3">
				<div class="col-md-3 widget widget1">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-universal-access icon-rounded"></i>
						<div class="stats">
						 <h5><strong><?php echo $jml_mahasiswa; ?></strong></h5>
						  <span>Total Daftar Mahasiswa</span>
						</div>
					</div>
				</div>
				<div class="col-md-3 widget widget1">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-users dollar2 icon-rounded"></i>
						<div class="stats">
						  <h5><strong><?php echo $jml_result; ?></strong></h5>
						  <span>Hasil Data Mahasiswa</span>
						</div>
					</div>
				</div>
				<div class="col-md-3 widget widget1">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-check-square-o user1 icon-rounded"></i>
						<div class="stats">
						  <h5><strong><?php echo $jml_verifikasi; ?></strong></h5>
						  <span>Menunggu Verifikasi</span>
						</div>
					</div>
				</div>
				<div class="col-md-3 widget widget1">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-user-md icon-rounded"></i>
						<div class="stats">
						  <h5><strong><?php echo $jml_konseling; ?></strong></h5>
						  <span>Total Daftar Konseling</span>
						</div>
					</div>
				 </div>
				<div class="col-md-3 widget">
					<div class="r3_counter_box">
						<i class="pull-left fa fa-book dollar2 icon-rounded"></i>
						<div class="stats">
						  <h5><strong><?php echo $jml_kamus; ?></strong></h5>
						  <span>Total Data Kamus</span>
						</div>
					</div>
				 </div>
				<div class="clearfix"> </div>
				</div>
			</div>
			<div class="clearfix"> </div>
			
			<div class="agileinfo-cdr">
				<!-- START GRAFIK -->
				<div class="charts">
					<div class="col-md-12">
						<div class="card-header">
							<h3>Grafik Hasil Data Mahasiswa</h3>
							<div class="clearfix"> </div>
						</div>
						<br />
						<br />
						<div id="container" >
							<canvas id="canvas"></canvas>
						</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<!-- END GRAFIK -->
			</div>
		</div>
	</div>
</div>



<script src="<?php echo base_url();?>assets/js/Chart.bundle.js"></script>
    <script src="<?php echo base_url();?>assets/js/utils.js"></script>
	
	<script>
        var color = Chart.helpers.color;
        var barChartData = {
            labels: ["Normal","Depresi","Depresi Psikotik","Depresi Dysthymia","Depresi Atypical","Depresi Mayor"],
            datasets: [{
                label: 'Jumlah Orang',
                backgroundColor: color(window.chartColors.blue).alpha(0.5).rgbString(),
                borderColor: window.chartColors.blue,
                borderWidth: 1,
                data: [
                    <?php echo $nilai1[0]->jumlah; ?>,
                    <?php echo $nilai2[0]->jumlah; ?>,
                    <?php echo $nilai3[0]->jumlah; ?>,
                    <?php echo $nilai4[0]->jumlah; ?>,
                    <?php echo $nilai5[0]->jumlah; ?>,
					<?php echo $nilai6[0]->jumlah; ?>
                ]
            }
			]

        };

        window.onload = function() {
            var ctx = document.getElementById("canvas").getContext("2d");
            window.myBar = new Chart(ctx, {
                type: 'bar',
                data: barChartData,
                options: {
                    responsive: true,
                    legend: {
                        position: 'bottom',
                    },
                    //title: {
                    //    display: true,
                    //    text: 'Chart.js Bar Chart'
                    //}
                }
            });

        };

    </script>
	<!-- new added graphs chart js-->