<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms">
			<div class="form-title">
				<h4>Form Keluhan</h4>
				<div class="clearfix"></div>
			</div>
			<div class="form-body">
				<form class="form-horizontal" action="<?php echo base_url() ?>beranda/save_jawaban_esay" method="post">
					<div><textarea class="ckeditor" name="keterangan" /></textarea></div>
					<br />
					<div class="form-group">
						<div class="col-sm-9">
							<button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> Simpan</button>
							<div class="clearfix"> </div>
						</div>
					</div>
				</form>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>
