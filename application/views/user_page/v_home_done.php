<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			
			<div class="panel-info widget-shadow">
				<!-- START DATA CONTENT -->
				<div class="col-md-12 panel-grids">
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h3 class="panel-title">INFORMASI</h3>
							<div class="clearfix"></div>
						</div>
						<div class="panel-body">
							<h4 style="text-align:center;">
								Terima kasih telah berpartisipasi dalam<br />
								Sistem Informasi Pendeteksi Depresi pada Mahasiswa<br />
								Fakultas Ilmu Komputer<br />
								Universitas Brawijaya<br/>
								Malang
								<br />
							</h4>
							<br />
							<br />
							<p>
								Berikut adalah hasil bimbingan konseling yang telah Anda lakukan melalui SIPSI.KOM<br />
								Hasil : Anda Terdeteksi  <?php echo $data[0]->jenis; ?><br />
								Solusi : <?php echo $data[0]->solusi; ?>
							<p>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
				<!-- END DATA CONTENT -->
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>