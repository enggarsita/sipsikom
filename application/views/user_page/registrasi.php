<!DOCTYPE HTML>
<html>
<head>
<title>SIPSI.KOM</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='<?php echo base_url(); ?>assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts-->
 
<!-- Metis Menu -->
<script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

</head>

<body >
<div class="main-content">
	
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page signup-page">
				<h2 class="title1">Registrasi Mahasiswa<br />SIPSI.KOM</h2>
				<div class="sign-up-row widget-shadow">
					<form class="form-horizontal" action="<?php echo base_url() ?>formulir/cek" method="post">
						<!-- START DATA DIRI Mahasiswa -->
						<div class="form-group">
							<label class="col-sm-2 control-label">NIM</label>
							<div class="col-sm-9">
								<input type="text" name="nim" id="nim" class="form-control" maxlength="16" placeholder="Nomor Induk Mahasiswa" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Nama</label>
							<div class="col-sm-9">
								<input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Lengkap" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Alamat</label>
							<div class="col-sm-9">
								<input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat Sesuai KTP" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Provinsi</label>
							<div class="col-sm-9">
								<select name='id_provinsi' id='id_provinsi' data-widget='select2' class='multisel' style ='width:100%; padding-bottom:5px;padding-top:5px;' onchange="provinsi_change();">
									<option value=''>- Provinsi -</option>
									<?php
										foreach($provinsi as $key=>$data){
											$wok=$data['nm_provinsi'];
											$id_wok=$data['id_provinsi'];
											echo "<option value='".$id_wok."'>".ucwords($wok)."</option>";
										}
									?>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Kota / Kabupaten</label>
							<div class="col-sm-9">
								<select name='id_kabupaten' id='id_kabupaten' data-widget='select2' class='multisel' style ='width:100%; padding-bottom:5px;padding-top:5px;'>
									<option value=''>- Kota / Kabupaten -</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Jurusan</label>
							<div class="col-sm-9">
								<input type="text" name="jurusan" id="jurusan" class="form-control" placeholder="Jurusan" required />
							</div>
						</div>
						<div class="form-group">
							<label class="col-sm-2 control-label">Password</label>
							<div class="col-sm-9">
								<input type="password" id="password-field" name="password" class="form-control" placeholder="Password" required />
								<input type="checkbox" id="form-checkbox"> Show Password
								<div class="clearfix"> </div>
							</div>
						</div>
						<!-- END DATA DIRI Mahasiswa -->
						
						<div align="center">
							<button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> Registrasi</button>
							<div class="clearfix"> </div>
						</div>
						<div class="registration">
							Sudah Registrasi Akun ?<a class="" href="daftar"> Masuk</a>
							<div class="clearfix"> </div>
						</div>
					</form>
				</div>
			</div>
			
		</div>
		<!--footer-->
		<div class="footer">
		   <p>&copy; 2019 <a href="<?php echo base_url(); ?>">SIPSI.KOM</a></p>
		</div>
        <!--//footer-->
	</div>
	
	<!--scrolling js-->
	<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"> </script>
	<!-- //Bootstrap Core JavaScript -->
   
</body>
</html>

<script>
$(document).ready(function(){		
		$('#form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('#password-field').attr('type','text');
			}else{
				$('#password-field').attr('type','password');
			}
		});
	});
	
	function provinsi_change() {
		var id_prov = document.getElementById("id_provinsi").value;
		
		$.ajax({
			url : "<?php echo base_url();?>formulir/list_kabupaten",
			method : "POST",
			data : {id_prov: id_prov},
			async : false,
			dataType : 'json',
			success: function(data){
				var html = '';
				var i;
				for(i=0; i<data.length; i++){
					html += '<option value="'+data[i].id_kabupaten+'">'+data[i].nm_kabupaten+'</option>';
				}
				$('#id_kabupaten').html(html);
			}
		});
	}
	
</script>