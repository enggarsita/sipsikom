<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			<div class="form-title">
				<h4>Ganti Kata Sandi</h4>
				<div class="clearfix"></div>
			</div>
			<div class="form-body">
				<!-- START CONTENT -->
				<form class="form-horizontal" action="<?php echo base_url() ?>beranda/reset_pass" method="post">
					<div class="form-group">
						<label class="col-sm-2 control-label">Kata Sandi Baru</label>
						<div class="col-sm-9">
							<input type="password" name="password" id="password-field" class="form-control" required />
							<input type="checkbox" id="form-checkbox"> Tampilkan Kata Sandi
							<div class="clearfix"> </div>
						</div>
					</div>
					
					<input type="hidden" name="nim" value="<?php echo $data->nim; ?>" />
					<input type="hidden" name="nama" value="<?php echo $data->nama; ?>" />

					<div class="form-group">
						<label class="col-sm-2 control-label"></label>
						<div class="col-sm-9">
							<button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-refresh"></i> Reset Kata Sandi</button>
							<div class="clearfix"> </div>
						</div>
					</div>					
				</form>
				<!-- END CONTENT -->
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<script>
$(document).ready(function(){		
		$('#form-checkbox').click(function(){
			if($(this).is(':checked')){
				$('#password-field').attr('type','text');
			}else{
				$('#password-field').attr('type','password');
			}
		});
	});
</script>