<!DOCTYPE HTML>
<html>
<head>
<title>SIPSI.KOM</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='<?php echo base_url(); ?>assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="<?php echo base_url(); ?>assets/js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

<!-- start datepicker -->
<script src="<?php echo base_url(); ?>assets/datepicker/js/bootstrap-datepicker.js"></script>
<link href="<?php echo base_url(); ?>assets/datepicker/css/bootstrap-datepicker.css" rel="stylesheet">
<!-- end datepicker -->

<!-- start ckeditor -->
<script type="text/javascript" src="<?php echo base_url(); ?>assets/ckeditor/ckeditor.js"></script>
<!-- end ckeditor -->

<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>assets/js/bootstrap.js"> </script>
<!-- //Bootstrap Core JavaScript -->

<!-- requried-jsfiles-for owl -->
<link href="<?php echo base_url(); ?>assets/css/owl.carousel.css" rel="stylesheet">
<script src="<?php echo base_url(); ?>assets/js/owl.carousel.js"></script>
<script>
	$(document).ready(function() {
		$("#owl-demo").owlCarousel({
			items : 3,
			lazyLoad : true,
			autoPlay : true,
			pagination : true,
			nav:true,
		});
	});
</script>
<!-- //requried-jsfiles-for owl -->
</head> 

<body>
	<div class="main-content">
	<div class="main-page">
		<div class="forms">
			<div class="form-grids row form-grids-right">
				<div class="widget-shadow " data-example-id="basic-forms">
					<div class="form-title">
						<h3>Data Mahasiswa</h3>
					</div>
					<div class="form-body">
						<form class="form-horizontal" action="<?php echo base_url() ?>formulir/cek" method="post">
							<!-- START DATA DIRI Mahasiswa -->
							<div class="form-group">
								<label class="col-sm-2 control-label">NIM</label>
								<div class="col-sm-9">
									<input type="text" name="nim" id="nim" class="form-control" maxlength="16" placeholder="Nomor Induk Mahasiswa" required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Nama</label>
								<div class="col-sm-9">
									<input type="text" name="nama" id="nama" class="form-control" placeholder="Nama Lengkap" required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Alamat</label>
								<div class="col-sm-9">
									<input type="text" name="alamat" id="alamat" class="form-control" placeholder="Alamat Sesuai KTP" required />
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label"></label>
								<div class="col-sm-9">
									<select name='id_provinsi' id='id_provinsi' data-widget='select2' class='multisel' style ='width:100%; padding-bottom:5px;padding-top:5px;' onchange="provinsi_change();">
										<option value=''>- Provinsi -</option>
										<?php
											foreach($provinsi as $key=>$data){
												$wok=$data['nm_provinsi'];
												$id_wok=$data['id_provinsi'];
												echo "<option value='".$id_wok."'>".ucwords($wok)."</option>";
											}
										?>
									</select>
								</div>
							</div>
							<!--input type="text" name="val_prov" id="val_prov" class="form-control" required /-->
							<div class="form-group">
								<label class="col-sm-2 control-label"></label>
								<div class="col-sm-9">
									<select name='id_kabupaten' id='id_kabupaten' data-widget='select2' class='multisel' style ='width:100%; padding-bottom:5px;padding-top:5px;'>
										<option value=''>- Kota / Kabupaten -</option>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-2 control-label">Jurusan</label>
								<div class="col-sm-9">
									<input type="text" name="jurusan" id="jurusan" class="form-control" placeholder="Jurusan" required />
								</div>
							</div>
							<br />
							<!-- END DATA DIRI Mahasiswa -->
							
							<div align="center">
								<button type="submit" class="btn btn-lg btn-warning"><i class="fa fa-save"></i> Registrasi</button>
								<div class="clearfix"> </div>
							</div>
							<div class="registration">
								Sudah Registrasi Akun ?<a class="" href="login"> Masuk</a>
								<div class="clearfix"> </div>
							</div>
							
						</form> 
					</div>
				</div>
			</div>
		</div>
	</div>
<?php $this->load->view('slice/v_footer'); ?>
</div>
</body>
</html>

<script>
	function provinsi_change() {
		var id_prov = document.getElementById("id_provinsi").value;
		
		$.ajax({
			url : "<?php echo base_url();?>formulir/list_kabupaten",
			method : "POST",
			data : {id_prov: id_prov},
			async : false,
			dataType : 'json',
			success: function(data){
				var html = '';
				var i;
				for(i=0; i<data.length; i++){
					html += '<option value="'+data[i].id_kabupaten+'">'+data[i].nm_kabupaten+'</option>';
				}
				$('#id_kabupaten').html(html);
			}
		});
	}
	
</script>