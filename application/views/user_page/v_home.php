<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms">
			<div class="form-title">
				<h4>Form Pertanyaan</h4>
				<div class="clearfix"></div>
			</div>
			<div class="form-body">
				<form class="form-horizontal" action="<?php echo base_url() ?>beranda/save_jawaban" method="post">
					<!-- START CONTENT -->
					<?php
					$i = 1;
					foreach ($list_pertanyaan as $list) {
					?>
					<p><?php echo $i.". ".$list['pertanyaan']; ?></p>
					<input type="hidden" id="id" name="id<?php echo $list['id']; ?>" class="form-control" value="<?php echo $list['id']; ?>" readonly />
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input name="radio<?php echo $list['id']; ?>" type="radio" value="<?php echo $list['skor']; ?>" required /> Ya&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input name="radio<?php echo $list['id']; ?>" type="radio" value="0"> Tidak
					<br />
					<?php $i++; ?>
					<?php } ?>
					<!-- END CONTENT -->
					<br />
					<div class="form-group">
						<div class="col-sm-9">
							<button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-save"></i> Simpan</button>
							<div class="clearfix"> </div>
						</div>
					</div>
				</form>
				<div class="clearfix"></div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>

