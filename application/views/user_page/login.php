<!DOCTYPE HTML>
<html>
<head>
<title>SIPSI.KOM</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='<?php echo base_url(); ?>assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts-->
 
<!-- Metis Menu -->
<script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

</head>

<body >
<div class="main-content">
	<div class="profile_details">
		<ul>
			<li class="dropdown profile_details_drop">
				<div class="profile_img">	
					<a href="<?php echo base_url(); ?>"><span class="prfil-img"><img src="<?php echo base_url(); ?>assets/images/icon/homee.png" alt=""></span></a>
				</div>
			</li>
		</ul>
	</div>
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page login-page">
				<h2 class="title1">Mahasiswa<br />SIPSI.KOM</h2>
				<div class="widget-shadow">
					<div class="login-body">
						<form action="<?php echo base_url() ?>daftar/cek" method="post">
							<input type="username" style="outline:none; border: 1px solid #D0D0D0; font-size: 15px; padding: 14px 15px 14px 37px; width:100%; margin: 0em 0 1.5em 0;" class="user" name="nim" placeholder="Masukkan NIM" required />
							<input type="password" class="lock" name="password" placeholder="Masukkan Kata Sandi" required />
							<div class="forgot-grid">
								<div class="forgot">
									<a href="daftar/forgot">lupa kata sandi?</a>
								</div>
								<div class="clearfix"> </div>
							</div>
							<div align="center">
								<button type="submit" class="btn btn-lg btn-primary"><i class="fa fa-sign-in"></i> Masuk</button>
								<div class="clearfix"> </div>
							</div>
							<div class="registration">
								Tidak Memiliki Akun ?
								<a class="" href="formulir">Daftar Akun</a>
								<div class="clearfix"> </div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<div class="footer">
		   <p>&copy; 2019 <a href="<?php echo base_url(); ?>">SIPSI.KOM</a></p>
		</div>
        <!--//footer-->
	</div>
	
	<!--scrolling js-->
	<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"> </script>
	<!-- //Bootstrap Core JavaScript -->
   
</body>
</html>