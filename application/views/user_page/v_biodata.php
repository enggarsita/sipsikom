<div class="forms">
	<div class=" form-grids row form-grids-right">
		<div class="widget-shadow " data-example-id="basic-forms"> 
			<div class="form-title">
				<h4>Biodata Mahasiswa</h4>
				<div class="clearfix"></div>
			</div>
			<div class="form-body">
				<!-- START CONTENT -->
				<form class="form-horizontal">
					<!-- START DATA DIRI PEGAWAI -->
					<div class="form-group">
						<label class="col-sm-2 control-label">NIM</label>
						<div class="col-sm-9">
							<input type="text" name="nim" id="nim" class="form-control" maxlength="15" value="<?php echo $data->nim; ?>" readonly />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Nama Lengkap</label>
						<div class="col-sm-9">
							<input type="text" name="nama" id="nama" class="form-control" value="<?php echo $data->nama; ?>" readonly />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Alamat</label>
						<div class="col-sm-9">
							<input type="text" name="alamat" id="alamat" class="form-control" value="<?php echo $data->alamat; ?>" readonly />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Kota / Kabupaten</label>
						<div class="col-sm-9">
							<input type="text" name="kabupaten" id="kabupaten" class="form-control" value="<?php echo $kabupaten; ?>" readonly />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Provinsi</label>
						<div class="col-sm-9">
							<input type="text" name="provinsi" id="provinsi" class="form-control" value="<?php echo $provinsi; ?>" readonly />
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label">Jurusan</label>
						<div class="col-sm-9">
							<input type="text" name="jurusan" id="jurusan" class="form-control" value="<?php echo $data->jurusan; ?>" readonly />
						</div>
					</div>
				</form>
				<!-- END CONTENT -->
			</div>
		</div>
		<div class="clearfix"></div>
	</div>
</div>