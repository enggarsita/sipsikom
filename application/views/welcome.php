<!DOCTYPE HTML>
<html>
<head>
<title>SIPSI.KOM</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>assets/css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='<?php echo base_url(); ?>assets/css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="<?php echo base_url(); ?>assets/js/jquery-1.11.1.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts-->
 
<!-- Metis Menu -->
<script src="<?php echo base_url(); ?>assets/js/metisMenu.min.js"></script>
<script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
<link href="<?php echo base_url(); ?>assets/css/custom.css" rel="stylesheet">
<!--//Metis Menu -->

</head>

<body >
<div class="main-content">
	
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page login-page">
				<h2 class="title1">Selamat Datang di<br />Sistem Informasi Pendeteksi Depresi<br />Fakultas Ilmu Komputer<br />Universitas Brawijaya Malang</h2>
				<div class="widget-shadow">
					<div class="login-body">
						<div align="center">
							<button type="button" id="konseling" class="btn btn-lg btn-primary" style="font-size: 20px; padding: 10px 40px;"><i class="fa fa-user-md"></i> Pembimbing Konseling</button>
							<div class="clearfix"> </div>
						</div>
						<br />
						<br />
						<div align="center">
							<button type="button" id="mahasiswa" class="btn btn-lg btn-primary" style="font-size: 20px; padding: 10px 95px;"><i class="fa fa-users"></i> Mahasiswa</button>
							<div class="clearfix"> </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--footer-->
		<div class="footer">
		   <p>&copy; 2019 <a href="<?php echo base_url(); ?>">SIPSI.KOM</a></p>
		</div>
        <!--//footer-->
	</div>
	
	<!--scrolling js-->
	<script src="<?php echo base_url(); ?>assets/js/jquery.nicescroll.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"> </script>
	<!-- //Bootstrap Core JavaScript -->
   
</body>
</html>

<script>
	$( "#konseling" ).click(function() {
		window.location.replace('<?php echo base_url(); ?>Login');
	});
	
	$( "#mahasiswa" ).click(function() {
		window.location.replace('<?php echo base_url(); ?>Daftar');
	});
</script>