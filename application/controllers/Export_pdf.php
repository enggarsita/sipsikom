<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Export_pdf extends CI_Controller {

    /*
    var $CI = NULL;
	public function __construct() {		 
		$this->CI =& get_instance();
	}
    */
	public function index() {        		
        $this->load->library('cezpdf');		
		$this->load->helper('pdf');
	    prep_pdf("user",'PT XYZ');
		$data[] = array('title' => 'Data 1', 'content' =>'Data 1');
        $data[] = array('title' => 'Data 2', 'content' =>'Data 2');
        $data[] = array('title' => 'Data 3', 'content' =>'Data 3');
        $data[] = array('title' => 'Data 4', 'content' =>'Data 4');
        $data[] = array('title' => 'Data 5', 'content' =>'Data 5');
        $data[] = array('title' => 'Data 6', 'content' =>'Data 6');
        $col_names = array(
            'title'	=> '',
            'content' => ''
        );	
        $this->cezpdf->ezTable($data, $col_names, 'Data Result', array('width'=>550));			
        $this->cezpdf->ezStream();		
        
        /*
        $data = array(	'title'	=> 'Export Page');
        $this->load->view('export_view',$data);	
        */
	} 
}