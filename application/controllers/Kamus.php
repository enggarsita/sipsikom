<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Kamus extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if($this->session->userdata('islogin')=='1'){
			$this->load->model('M_Master');
			$this->load->model('M_Menu');
		} elseif($this->session->userdata('islogin')=='2'){
			redirect('beranda/solusi');
		}else {
			redirect('welcome');
		}
	}
	
	public function index()
	{
		$menu = $this->M_Menu->get_menu();
		$dt_kamus = $this->M_Master->get_list_kamus();
		
		$data_hal = array ("container" => "admin_page/v_kamus","menus" => $menu,"list_kamus"=>$dt_kamus);
		
		$this->load->view('admin_page/template',$data_hal);
	}
	
	public function add()
	{
		$menu = $this->M_Menu->get_menu();
		
		$data_hal = array ("container" => "admin_page/v_kamus_add","menus" => $menu);
		
		$this->load->view('admin_page/template',$data_hal);
	}
	
	public function save_add()
	{
		$judul		= addslashes($this->input->post('judul'));
		$isi		= addslashes($this->input->post('isi'));
		$date_now	= date('Y-m-d H:i:s');
		
		if($isi!=''){
			$save = $this->M_Master->add_kamus($judul, $isi, $date_now);
		
			if(!$save){
				echo"<script type='text/javascript'>alert('Data Kamus Berhasil di Tambahkan!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."kamus';</script>";
			}else{
				echo"<script type='text/javascript'>alert('Data Kamus Gagal di Tambahkan!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."kamus';</script>";
			}
		}else {
			echo"<script type='text/javascript'>alert('Gagal, Silahkan Lengkapi Data!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."kamus';</script>";
		}
		
	}
	
	public function view($id=false)
	{
		$id = addslashes($id);
		
		$menu = $this->M_Menu->get_menu();
		$list_view = $this->M_Master->search_kamus($id);
		
		$data_hal = array ("container" => "admin_page/v_kamus_view","menus" => $menu, "dt_view" => $list_view);
		
		$this->load->view('admin_page/template',$data_hal);
	}
	
	public function edit($id=false)
	{
		$id = addslashes($id);
		
		$menu = $this->M_Menu->get_menu();
		$list_edit = $this->M_Master->search_kamus($id);
		
		$data_hal = array ("container" => "admin_page/v_kamus_edit","menus" => $menu, "dt_edit" => $list_edit);
		
		$this->load->view('admin_page/template',$data_hal);
		
	}
	
	public function save_edit()
	{
		$id			= addslashes($this->input->post('id'));
		$judul		= addslashes($this->input->post('judul'));
		$isi		= addslashes($this->input->post('isi'));
		$date_now	= date('Y-m-d H:i:s');
		
		if($isi!=''){
			$edit = $this->M_Master->edit_kamus($id, $judul, $isi, $date_now);
			
			if(!$edit){
				echo"<script type='text/javascript'>alert('Data Kamus Berhasil di Perbarui!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."kamus';</script>";
			}else{
				echo"<script type='text/javascript'>alert('Data Kamus Gagal di Perbarui!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."kamus';</script>";
			}
		} else {
			echo"<script type='text/javascript'>alert('Gagal, Silahkan Lengkapi Data!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."kamus';</script>";
		}
	}
	
	public function hapus($id=false)
	{
		$id = addslashes($id);
		
		$menu = $this->M_Menu->get_menu();
		$list_delete = $this->M_Master->search_kamus($id);
		
		$id = $list_delete[0]->id;
		
		if($id!=''){
			$delete = $this->M_Master->delete_kamus($id);
			
			echo"<script type='text/javascript'>alert('Data Kamus Berhasil Dihapus!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."kamus';</script>";
		}else{
			echo"<script type='text/javascript'>alert('Data Kamus Gagal Dihapus!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."kamus';</script>";
		}
		
	}
	
}
