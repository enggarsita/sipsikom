<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Beranda extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if($this->session->userdata('islogin')=='1'){
			redirect('home');
		} elseif($this->session->userdata('islogin')=='2'){
			$this->load->model('M_form');
			$this->load->model('M_wilayah');
		}else {
			redirect('welcome');
		}
	}
	
	public function index()
	{
		$nim = $this->session->userdata('nim');
		
		$cek_jwb_esay = $this->M_form->get_jwb_esay_mhs($nim);
		
		if(!$cek_jwb_esay){
			$data_hal = array ("container" => "user_page/v_home1");
			$this->load->view('user_page/template',$data_hal);
		} else {
			$cek_jwb = $this->M_form->get_jwb_mhs($nim);
		
			if(!$cek_jwb){
				$dt_pertanyaan = $this->M_form->get_list_pertanyaan();
				
				$data_hal = array ("container" => "user_page/v_home","list_pertanyaan"=>$dt_pertanyaan);
				$this->load->view('user_page/template',$data_hal);
			}else{
				echo"<script type='text/javascript'>window.location='".base_url()."beranda/solusi';</script>";
			}
		}
		
		
	}
	
	public function save_jawaban_esay()
	{
		$nim = $this->session->userdata('nim');
		$keterangan = addslashes($this->input->post('keterangan'));
		$date_now = date('Y-m-d H:i:s');
		
		if($keterangan){
			$save = $this->M_form->save_jawaban_esay($nim, $keterangan, $date_now);
			
			$dt_pertanyaan = $this->M_form->get_list_pertanyaan();
				
			$data_hal = array ("container" => "user_page/v_home","list_pertanyaan"=>$dt_pertanyaan);
			$this->load->view('user_page/template',$data_hal);
			
		} else {
			echo"<script type='text/javascript'>alert('Lengkapi Data!!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."beranda';</script>";
			exit();
		}
	}
	
	public function save_jawaban()
	{
		$nim = $this->session->userdata('nim');
		$date_now = date('Y-m-d H:i:s');
		
		$dt_pertanyaan = $this->M_form->get_list_pertanyaan();

		$i = 1;
		foreach ($dt_pertanyaan as $dt) {
			$no = $dt_pertanyaan[$i]['id'];
			$id_no = addslashes($this->input->post('id'.$no));
			$radio = addslashes($this->input->post('radio'.$no));
			$i++;
			$save = $this->M_form->save_jawaban($nim, $id_no, $radio, $date_now);
        }
		
		echo"<script type='text/javascript'>alert('Terima Kasih, Data Berhasil di Simpan!!!')</script>";
		echo"<script type='text/javascript'>window.location='".base_url()."beranda/solusi';</script>";
	}
	
	public function solusi()
	{
		$nim = $this->session->userdata('nim');
		$dt_solusi = $this->M_form->jumlah_skor($nim);
		
		$jml_solusi = $dt_solusi[0]->skor;
		
		if($jml_solusi<='2'){
			$id = '1';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='8') {
			$id = '2';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='24') {
			$id = '3';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='42') {
			$id = '4';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='70') {
			$id = '5';
			$solusi = $this->M_form->solusi($id);
		} else {
			$id = '6';
			$solusi = $this->M_form->solusi($id);
		}
		
		$data_hal = array ("container" => "user_page/v_home_done", "data" => $solusi);
		$this->load->view('user_page/template',$data_hal);
	}
	
	public function biodata()
	{
		$nim = $this->session->userdata('nim');
		
		$cek_data = $this->M_form->get_data_mhs($nim);
		
		$id_provinsi = $cek_data[0]->id_provinsi;
		$id_kabupaten = $cek_data[0]->id_kabupaten;
		
		$cek_provinsi = $this->M_wilayah->get_idprovinsi($id_provinsi);
		$provinsi = $cek_provinsi[0]->nm_provinsi;
		
		$cek_kabupaten = $this->M_wilayah->get_idkabupaten($id_kabupaten);
		$kabupaten = $cek_kabupaten[0]->nm_kabupaten;
		
		$data_hal = array ("container" => "user_page/v_biodata", "data"=>$cek_data[0], "provinsi"=>$provinsi, "kabupaten"=>$kabupaten);
		
		$this->load->view('user_page/template',$data_hal);
	}
	
	public function ganti_pass()
	{
		$nim = $this->session->userdata('nim');
		
		$cek_data = $this->M_form->get_data_mhs($nim);
		
		$data_hal = array ("container" => "user_page/v_password", "data"=>$cek_data[0]);
		
		$this->load->view('user_page/template',$data_hal);
	}
	
	public function reset_pass()
	{
		$nim = addslashes($this->input->post('nim'));
		$nama = addslashes($this->input->post('nama'));
		$pass = addslashes(md5($this->input->post('password')));
		$date_now = date('Y-m-d H:i:s');
		
		if ($nim and $nama and $pass) {
			$reset_pass = $this->M_form->save_pass($nim, $nama, $pass, $date_now);
			
			echo"<script type='text/javascript'>alert('Password Berhasil di Perbarui!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."beranda';</script>";
		} else {
			echo"<script type='text/javascript'>alert('Lengkapi Data!!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."beranda/ganti_pass';</script>";
			exit();
		}
	}
	
}
