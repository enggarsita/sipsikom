<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Login extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_master');
	}
	
	public function index()
	{
		$islogin		= $this->session->userdata('islogin');
		
		if($this->session->userdata('islogin')=='1'){
			redirect('home');
		} elseif($this->session->userdata('islogin')=='2'){
			redirect('beranda/solusi');
		}else {
			$this->load->helper(array('form', 'html'));
			$this->load->view('admin_page/login');
		}
	}
	
	public function cek()
	{
		$user = addslashes($this->input->post('username'));
		$pass = addslashes(md5($this->input->post('password')));
		
		if ($user and $pass) {
			$cek_credential = $this->M_master->get_user($user, $pass);
			
			if($cek_credential){
				$this->session->set_userdata('username',$cek_credential[0]->username);
				
				$this->session->set_userdata('islogin','1');
				
				redirect("home");
			} else{
				echo"<script type='text/javascript'>alert('Username / Password Salah!!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."login';</script>";
				exit();
			}
		} else {
			redirect('admin_page/login?err=nopass');
		}
	}
	
	public function logout()
	{
		unset($_SESSION["username"]);
		unset($_SESSION["islogin"]);
		session_unset();
		session_destroy();
		echo"<script type='text/javascript'>window.location='".base_url()."login';</script>";
		
	}
	
}
