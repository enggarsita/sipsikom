<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Welcome extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_master');
	}
	
	public function index()
	{
		$islogin		= $this->session->userdata('islogin');
		
		if($this->session->userdata('islogin')=='1'){
			redirect('home');
		} elseif($this->session->userdata('islogin')=='2'){
			redirect('beranda/solusi');
		}else {
			$this->load->helper(array('form', 'html'));
			$this->load->view('welcome');
		}
	}
}