<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Forgot extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_master');
	}
	
	public function index()
	{
		$islogin		= $this->session->userdata('islogin');
		
		if($this->session->userdata('islogin')=='1'){
			redirect('home');
		} elseif($this->session->userdata('islogin')=='2'){
			redirect('beranda/solusi');
		}else {
			$this->load->helper(array('form', 'html'));
			$this->load->view('admin_page/forgot');
		}
	}
	
	public function cek()
	{
		$user = addslashes($this->input->post('username'));
		$no_hp = addslashes($this->input->post('nohp'));
		
		if ($user and $no_hp) {
			$cek_data = $this->M_master->get_cek_user($user, $no_hp);
			
			if($cek_data){
				$data['nama'] = $cek_data[0]->username;
				$data['nohp'] = $cek_data[0]->no_hp;
				
				$this->load->view('admin_page/reset', $data);
				
			} else{
				echo"<script type='text/javascript'>alert('Username / Nomor Handphone Salah!!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."forgot';</script>";
				exit();
			}
			
		} else {
			echo"<script type='text/javascript'>alert('Lengkapi Data!!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."forgot';</script>";
			exit();
		}
	}
	
	public function ganti_pass()
	{
		$user = addslashes($this->input->post('username'));
		$no_hp = addslashes($this->input->post('nohp'));
		$pass = addslashes(md5($this->input->post('password')));
		$date_now = date('Y-m-d H:i:s');
		
		if ($user and $no_hp and $pass) {
			$reset_pass = $this->M_master->save_pass($user, $no_hp, $pass, $date_now);
			
			echo"<script type='text/javascript'>alert('Password Berhasil di Perbarui!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."login';</script>";
		} else {
			echo"<script type='text/javascript'>alert('Lengkapi Data!!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."forgot';</script>";
			exit();
		}
	}
	
}
