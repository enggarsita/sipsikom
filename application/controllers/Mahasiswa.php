<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Mahasiswa extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if($this->session->userdata('islogin')=='1'){
			$this->load->model('M_Master');
			$this->load->model('M_Menu');
			$this->load->model('M_form');
			$this->load->model('M_wilayah');
			
			//$this->load->add_package_path( APPPATH . 'third_party/fpdf');
			$this->load->config('pdf_config');
			$this->load->library('pdf');
			$this->load->helper('date');
				define('FPDF_FONTPATH',$this->config->item('fonts_path'));

		} elseif($this->session->userdata('islogin')=='2'){
			redirect('beranda/solusi');
		}else {
			redirect('welcome');
		}
	}
	
	public function index()
	{
		$menu = $this->M_Menu->get_menu();
		$dt_result = $this->M_Master->get_list_result();
		
		$data_hal = array ("container" => "admin_page/v_hasil_data","menus" => $menu,"list_result"=>$dt_result);
		
		$this->load->view('admin_page/template',$data_hal);
	}
	
	//public function result()
	//{
	//	$menu = $this->M_Menu->get_menu();
	//	$dt_result = $this->M_Master->get_list_result();
	//	
	//	$data_hal = array ("container" => "admin_page/v_hasil_data","menus" => $menu,"list_result"=>$dt_result);
	//	
	//	$this->load->view('admin_page/template',$data_hal);
	//}
	
	public function download()
	{
		
		$listweeklywinner = $this->M_Master->get_list_result();

		// Load plugin PHPExcel nya
		include APPPATH . 'third_party/PHPExcel/PHPExcel.php';

		// Panggil class PHPExcel nya
		$excel = new PHPExcel();
		
		// Settingan awal file excel
		$excel->getProperties()->setCreator('SIPSI.KOM')
				->setLastModifiedBy('SIPSI.KOM')
				->setTitle("Notifikasi SIPSI.KOM")
				->setSubject("Notifikasi SIPSI.KOM")
				->setDescription("Notifikasi SIPSI.KOM")
				->setKeywords("SIPSI.KOM");
		// Buat sebuah variabel untuk menampung pengaturan style dari header tabel
		$style_col = array(
			'font' => array('bold' => true), // Set font nya jadi bold
			'alignment' => array(
				'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, // Set text jadi ditengah secara horizontal (center)
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN), // Set border right dengan garis tipis
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		// Buat sebuah variabel untuk menampung pengaturan style dari isi tabel
		$style_row = array(
			'alignment' => array(
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER // Set text jadi di tengah secara vertical (middle)
			),
			'borders' => array(
				'top' => array('style' => PHPExcel_Style_Border::BORDER_THIN), // Set border top dengan garis tipis
				'right' => array('style' => PHPExcel_Style_Border::BORDER_THIN), // Set border right dengan garis tipis
				'bottom' => array('style' => PHPExcel_Style_Border::BORDER_THIN), // Set border bottom dengan garis tipis
				'left' => array('style' => PHPExcel_Style_Border::BORDER_THIN) // Set border left dengan garis tipis
			)
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "Daftar Hasil Data Mahasiswa SIPSI.KOM"); // Set kolom A1 dengan tulisan "Daftar Hasil Data SIPSI.KOM"
		$excel->getActiveSheet()->mergeCells('A1:E1'); // Set Merge Cell pada kolom A1 sampai E1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); // Set bold kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); // Set font size 15 untuk kolom A1
		$excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER); // Set text center untuk kolom A1
		// Buat header tabel nya pada baris ke 3
		$excel->setActiveSheetIndex(0)->setCellValue('A3', "No."); // Set kolom A3 dengan tulisan "No"
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "NIM"); // Set kolom B3 dengan tulisan "NIM"
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "Nama"); // Set kolom C3 dengan tulisan "nama"
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "Jurusan"); // Set kolom C3 dengan tulisan "jurusan"
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "Tanggal Konseling"); // Set kolom D3 dengan tulisan "Tanggal Konseling"
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "Tanggal Verifikasi"); // Set kolom E3 dengan tulisan "Tanggal Verifikasi"
		// Apply style header yang telah kita buat tadi ke masing-masing kolom header
		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);

		$no = 1; // Untuk penomoran tabel, di awal set dengan 1
		$numrow = 4; // Set baris pertama untuk isi tabel adalah baris ke 4

		foreach ($listweeklywinner as $data) { // Lakukan looping pada variabel siswa
			$excel->setActiveSheetIndex(0)->setCellValue('A' . $numrow, $no);
			//$excel->setActiveSheetIndex(0)->setCellValue('B' . $numrow, "'".$data->pmm_mc_card_number);
			$excel->getActiveSheet()->setCellValueExplicit('B' . $numrow, $data['nim'], PHPExcel_Cell_DataType::TYPE_STRING);
			$excel->setActiveSheetIndex(0)->setCellValue('C' . $numrow, $data['nama']);
			$excel->setActiveSheetIndex(0)->setCellValue('D' . $numrow, $data['jurusan']);
			$excel->setActiveSheetIndex(0)->setCellValue('E' . $numrow, date('d-M-Y H:i:s', strtotime($data['created_date'])));
			$excel->setActiveSheetIndex(0)->setCellValue('F' . $numrow, date('d-M-Y H:i:s', strtotime($data['date_validasi'])));

			// Apply style row yang telah kita buat tadi ke masing-masing baris (isi tabel)
			$excel->getActiveSheet()->getStyle('A' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('B' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('C' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('D' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('E' . $numrow)->applyFromArray($style_row);
			$excel->getActiveSheet()->getStyle('F' . $numrow)->applyFromArray($style_row);

			$no++; // Tambah 1 setiap kali looping
			$numrow++; // Tambah 1 setiap kali looping
		}
		// Set width kolom
		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); // Set width kolom A
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(25); // Set width kolom B
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(50); // Set width kolom C
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(50); // Set width kolom D
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(50); // Set width kolom E
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(50); // Set width kolom E
		// Set height semua kolom menjadi auto (mengikuti height isi dari kolommnya, jadi otomatis)
		$excel->getActiveSheet()->getDefaultRowDimension()->setRowHeight(-1);
		// Set orientasi kertas jadi LANDSCAPE
		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
		// Set judul file excel nya
		$excel->getActiveSheet(0)->setTitle("Hasil Data Mahasiswa SIPSI.KOM");
		$excel->setActiveSheetIndex(0);
		// Proses file excel
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="hasil_data_mahasiswa.xlsx"'); // Set nama file excel nya
		header('Cache-Control: max-age=0');

		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');
	
	}
	
	public function view_result($nim=false)
	{
		$nim = addslashes($nim);
		
		$menu = $this->M_Menu->get_menu();
		$esay_view = $this->M_Master->search_data_verif_esay($nim);
		$list_view = $this->M_Master->search_data_verif($nim);
		
		$cek_data = $this->M_form->get_data_mhs($nim);
		
		$id_provinsi = $cek_data[0]->id_provinsi;
		$id_kabupaten = $cek_data[0]->id_kabupaten;
		
		$cek_provinsi = $this->M_wilayah->get_idprovinsi($id_provinsi);
		$provinsi = $cek_provinsi[0]->nm_provinsi;
		
		$cek_kabupaten = $this->M_wilayah->get_idkabupaten($id_kabupaten);
		$kabupaten = $cek_kabupaten[0]->nm_kabupaten;
		
		$dt_solusi = $this->M_form->jumlah_skor($nim);
		
		$jml_solusi = $dt_solusi[0]->skor;
		
		if($jml_solusi<='2'){
			$id = '1';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='8') {
			$id = '2';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='24') {
			$id = '3';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='42') {
			$id = '4';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='70') {
			$id = '5';
			$solusi = $this->M_form->solusi($id);
		} else {
			$id = '6';
			$solusi = $this->M_form->solusi($id);
		}
		
		$data_hal = array ("container" => "admin_page/v_hasil_data_view","menus" => $menu, "dt_esay" => $esay_view, "dt_view" => $list_view, "data"=>$cek_data[0], "provinsi"=>$provinsi, "kabupaten"=>$kabupaten, "data_solusi" => $solusi);
		
		$this->load->view('admin_page/template',$data_hal);
	}
	
	public function hapus_result($nim=false)
	{
		$nim = addslashes($nim);
		
		$menu = $this->M_Menu->get_menu();
		$list_delete = $this->M_Master->search_del_verif($nim);
		$list_delete_esay = $this->M_Master->search_del_verif_esay($nim);
		
		$id = $list_delete[0]->nim;
		$id_esay = $list_delete_esay[0]->nim;
		
		if($id!='' && $id_esay!=''){
			$delete = $this->M_Master->delete_verif($nim);
			$delete_esay = $this->M_Master->delete_verif_esay($nim);
			$list_delete_finish = $this->M_Master->search_del_finish($nim);
			
			echo"<script type='text/javascript'>alert('Data Berhasil Dihapus!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."mahasiswa';</script>";
		}else{
			echo"<script type='text/javascript'>alert('Data Gagal Dihapus!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."mahasiswa';</script>";
		}
	}
	
	public function verifikasi()
	{
		$menu = $this->M_Menu->get_menu();
		$dt_verifikasi = $this->M_Master->get_list_verifikasi();
		
		$data_hal = array ("container" => "admin_page/v_verifikasi","menus" => $menu,"list_verifikasi"=>$dt_verifikasi);
		
		$this->load->view('admin_page/template',$data_hal);
	}
	
	public function view_verif($nim=false)
	{
		$nim = addslashes($nim);
		
		$menu = $this->M_Menu->get_menu();
		$esay_view = $this->M_Master->search_data_verif_esay($nim);
		$list_view = $this->M_Master->search_data_verif($nim);
		
		$cek_data = $this->M_form->get_data_mhs($nim);
		
		$id_provinsi = $cek_data[0]->id_provinsi;
		$id_kabupaten = $cek_data[0]->id_kabupaten;
		
		$cek_provinsi = $this->M_wilayah->get_idprovinsi($id_provinsi);
		$provinsi = $cek_provinsi[0]->nm_provinsi;
		
		$cek_kabupaten = $this->M_wilayah->get_idkabupaten($id_kabupaten);
		$kabupaten = $cek_kabupaten[0]->nm_kabupaten;
		
		$dt_solusi = $this->M_form->jumlah_skor($nim);
		
		$jml_solusi = $dt_solusi[0]->skor;
		
		if($jml_solusi<='2'){
			$id = '1';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='8') {
			$id = '2';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='24') {
			$id = '3';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='42') {
			$id = '4';
			$solusi = $this->M_form->solusi($id);
		} elseif($jml_solusi<='70') {
			$id = '5';
			$solusi = $this->M_form->solusi($id);
		} else {
			$id = '6';
			$solusi = $this->M_form->solusi($id);
		}
		
		$data_hal = array ("container" => "admin_page/v_verifikasi_view","menus" => $menu, "dt_esay" => $esay_view, "dt_view" => $list_view, "data"=>$cek_data[0], "provinsi"=>$provinsi, "kabupaten"=>$kabupaten, "data_solusi" => $solusi);
		
		$this->load->view('admin_page/template',$data_hal);
	}
	
	public function edit_verif($nim=false)
	{
		$nim = addslashes($nim);
		$date_now	= date('Y-m-d H:i:s');
		
		if($nim){
			$edit = $this->M_Master->edit_verifikasi($nim, $date_now);
			$edit_esay = $this->M_Master->edit_verifikasi_esay($nim, $date_now);
			
			$dt_solusi = $this->M_form->jumlah_skor($nim);
		
			$jml_solusi = $dt_solusi[0]->skor;
			
			if($jml_solusi<='2'){
				$id = '1';
				$solusi = $this->M_form->solusi($id);
			} elseif($jml_solusi<='8') {
				$id = '2';
				$solusi = $this->M_form->solusi($id);
			} elseif($jml_solusi<='24') {
				$id = '3';
				$solusi = $this->M_form->solusi($id);
			} elseif($jml_solusi<='42') {
				$id = '4';
				$solusi = $this->M_form->solusi($id);
			} elseif($jml_solusi<='70') {
				$id = '5';
				$solusi = $this->M_form->solusi($id);
			} else {
				$id = '6';
				$solusi = $this->M_form->solusi($id);
			}
			
			$jenis = $solusi[0]->jenis;
			
			$save_end = $this->M_Master->save_jawaban_end($nim, $jenis, $jml_solusi, $date_now);
			
			if(!$edit and !$edit_esay){
				echo"<script type='text/javascript'>alert('Data Berhasil di Verifikasi!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."mahasiswa/verifikasi';</script>";
			}else{
				echo"<script type='text/javascript'>alert('Data Gagal di Verifikasi!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."mahasiswa/verifikasi';</script>";
			}
		}else{
			echo"<script type='text/javascript'>alert('Gagal Verifikasi!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."mahasiswa/verifikasi';</script>";
		}
	}
	
	public function hapus_verif($nim=false)
	{
		$nim = addslashes($nim);
		
		$menu = $this->M_Menu->get_menu();
		$list_delete = $this->M_Master->search_del_verif($nim);
		$list_delete_esay = $this->M_Master->search_del_verif_esay($nim);
		
		$id = $list_delete[0]->nim;
		$id_esay = $list_delete_esay[0]->nim;
		
		if($id!='' && $id_esay!=''){
			$delete = $this->M_Master->delete_verif($nim);
			$delete_esay = $this->M_Master->delete_verif_esay($nim);
			
			echo"<script type='text/javascript'>alert('Data Berhasil Dihapus!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."mahasiswa/verifikasi';</script>";
		}else{
			echo"<script type='text/javascript'>alert('Data Gagal Dihapus!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."mahasiswa/verifikasi';</script>";
		}
	}
		
	/*public function cetak_pdf($nim=false) 
	{        		
		$nim = addslashes($nim);
		
		$this->load->library('cezpdf');		
		$this->load->helper('pdf');
	    prep_pdf("user",'PT XYZ');
		
		$data = $this->M_Master->get_pdf($nim);

		$this->cezpdf->ezTable($data, $col_names, 'Data Result', array('width'=>550));
		$this->cezpdf->ezText("This example shows how to crypt the PDF document\n");
        $this->cezpdf->ezStream();
		
		//$fpdf->Output();
        
        /*
        $data = array(	'title'	=> 'Export Page');
        $this->load->view('export_view',$data);	
        */
	//}
	
	public function cetak_pdf($nim=false) 
	{        		
		//$nim = addslashes($nim);
		
		$data = $this->M_Master->get_pdf($nim);

		$this->pdf = new Pdf();
        $this->pdf->Add_Page('P','A4',0);
        $this->pdf->AliasNbPages();
		
		$this->pdf->SetFont('Times','',14);
		$this->pdf->Cell(00,10,'Malang, '. date("d F Y"),0,0,'R');
		
		$this->pdf->SetFont('Times','',14);
		$this->pdf->Ln(7);
		$this->pdf->Cell(00,30,'Kepada yang terhormat :',0,0,'J');
		$this->pdf->Ln(7);
		$this->pdf->Cell(00,35,'Effiana Yuriastien, S.Psi, Psikolog',0,0,'J');
		
		$this->pdf->SetFont('Times','',14);
		$this->pdf->Ln(7);
		$this->pdf->Cell(00,40,'RSJ. Dr. Radjiman Wediodiningrat Lawang',0,0,'J');
		$this->pdf->Ln(7);
		$this->pdf->Cell(00,45,'Klinik Psikologi',0,0,'J');
		
		$this->pdf->SetFont('Times','',14);
		$this->pdf->Ln(17);
		$this->pdf->Cell(00,50,'Dengan hormat,',0,0,'J');
		$this->pdf->Ln(7);
		$this->pdf->Cell(00,55,'Bersama datangnya surat ini, mohon dilakukan pemeriksaan psikologis terhadap mahasiswa',0,0,'J');
		$this->pdf->Ln(9);
		$this->pdf->Cell(00,55,'berikut :',0,0,'J');
		
		$this->pdf->Ln(12);
		$this->pdf->Cell(00,60,'Nama',0,0,'J');
		$this->pdf->Cell(-160);
		$this->pdf->Cell(00,60,': '.strtoupper($data[1]['nama']),0,1,'J');
		
		$this->pdf->Ln(9);
		$this->pdf->Cell(00,-60,'NIM',0,0,'J');
		$this->pdf->Cell(-160);
		$this->pdf->Cell(00,-60,': '.$data[1]['nim'],0,1,'J');
		
		$this->pdf->Ln(7);
		$this->pdf->Cell(00,64,'Indikasi',0,0,'J');
		$this->pdf->Cell(-160);
		$this->pdf->Cell(00,64,': '.strtoupper($data[1]['jenis']),0,1,'J');
		
		$this->pdf->Ln(9);
		$this->pdf->Cell(00,-64,'Keperluan',0,0,'J');
		$this->pdf->Cell(-160);
		$this->pdf->Cell(00,-64,': TINDAKAN LANJUT',0,1,'J');
		
		$this->pdf->Ln(12);
		$this->pdf->Cell(00,70,'Demikian dan terima kasih atas kerjasama yang diberikan.',0,0,'J');
		
		$this->pdf->Ln(18);
		$this->pdf->Cell(120);
		$this->pdf->Cell(00,80,'Ketua BK FILKOM',0,0,'C');
		
		$this->pdf->Ln(9);
		$this->pdf->Cell(120);
		$this->pdf->Cell(00,80,'Universitas Brawijaya',0,0,'C');
		
		$this->pdf->SetFont('Times','BU',14);
		$this->pdf->Ln(75);
		$this->pdf->Cell(122);
		$this->pdf->Cell(00,10,'(Wiwin Lukitohadi, S.Psi, S.H, CHRM)',0,0,'C');
		
		$this->pdf->SetFont('Times','',14);
		$this->pdf->Ln(10);
		$this->pdf->Cell(121);
		$this->pdf->Cell(00,00,'NIK. 2012037704302001',0,0,'C');
		
		//Times New Roman 15
		//$this->pdf->SetFont('Times','',12);
		//$this->pdf->Ln(5);
		//$this->pdf->Cell(80);
		//$this->pdf->Cell(30,10,0,0,'C');
		//pindah baris
		$this->pdf->Ln(5);
		//buat garis horisontal
		$this->pdf->Line(40,34,200,34);
        
        //cetak bentuk pdf
		$this->pdf->Output( 'Surat Pengantar_'. date("d F Y") .'.pdf', 'I' );
	}
}
