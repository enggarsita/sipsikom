<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Registrasi extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_master');
	}
	
	public function index()
	{
		$islogin		= $this->session->userdata('islogin');
		
		if($this->session->userdata('islogin')=='1'){
			redirect('home');
		} elseif($this->session->userdata('islogin')=='2'){
			redirect('beranda/solusi');
		}else {
			$this->load->helper(array('form', 'html'));
			$this->load->view('admin_page/registrasi');
		}
	}
	
	public function cek()
	{
		$nama = addslashes($this->input->post('nama'));
		$no_hp = addslashes($this->input->post('nohp'));
		$user = addslashes($this->input->post('username'));
		$pass = addslashes(md5($this->input->post('password')));
		$date_now = date('Y-m-d H:i:s');
		
		if($nama and $no_hp and $user and $pass){
			$cek_user = $this->M_master->get_cek_user_regis($user, $no_hp);
			
			if(!$cek_user)
			{
				$save_regis = $this->M_master->save_regis($nama, $no_hp, $user, $pass, $date_now);
				
				echo"<script type='text/javascript'>alert('Registrasi Sukses!!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."login';</script>";
			} else {
				echo"<script type='text/javascript'>alert('Registrasi Gagal, Data Sudah Digunakan!!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."registrasi';</script>";
			}
		} else {
			echo"<script type='text/javascript'>alert('Lengkapi Data!!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."registrasi';</script>";
			exit();
		}
		
		
	}
	
}
