<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Home extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		if($this->session->userdata('islogin')=='1'){
			$this->load->model('M_Master');
			$this->load->model('M_Menu');
			$this->load->model('M_Grafik');
		} elseif($this->session->userdata('islogin')=='2'){
			redirect('beranda/solusi');
		}else {
			redirect('welcome');
		}
	}
	
	public function index()
	{
		$menu = $this->M_Menu->get_menu();
		$jml_result = $this->M_Master->count_result();
		$length_result = count($jml_result);
		
		$jml_verifikasi = $this->M_Master->count_verifikasi();
		$length_verifikasi = count($jml_verifikasi);
		
		$jml_mahasiswa = $this->M_Master->count_mahasiswa();
		$length_mahasiswa = count($jml_mahasiswa);
		
		$jml_konseling = $this->M_Master->count_konseling();
		$length_konseling = count($jml_konseling);
		
		$jml_kamus = $this->M_Master->count_kamus();
		$length_kamus = count($jml_kamus);
		
		// START GRAFIK
		$nilai1 = $this->M_Grafik->get_nilai1();
		$nilai2 = $this->M_Grafik->get_nilai2();
		$nilai3 = $this->M_Grafik->get_nilai3();
		$nilai4 = $this->M_Grafik->get_nilai4();
		$nilai5 = $this->M_Grafik->get_nilai5();
		$nilai6 = $this->M_Grafik->get_nilai6();
		// END GRAFIK
		
		$data_hal = array (
			"container" => "admin_page/v_home",
			"menus" => $menu,
			"jml_result" => $length_result,
			"jml_verifikasi" => $length_verifikasi,
			"jml_mahasiswa" => $length_mahasiswa,
			"jml_konseling" => $length_konseling,
			"jml_kamus" => $length_kamus,
			"nilai1" => $nilai1,
			"nilai2" => $nilai2,
			"nilai3" => $nilai3,
			"nilai4" => $nilai4,
			"nilai5" => $nilai5,
			"nilai6" => $nilai6
		);
		
		$this->load->view('admin_page/template',$data_hal);
	}
	
}
