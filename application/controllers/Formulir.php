<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Formulir extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_wilayah');		
		$this->load->model('M_form');		
	}
	
	public function index()
	{
		$islogin		= $this->session->userdata('islogin');
		
		if($this->session->userdata('islogin')=='1'){
			redirect('home');
		} elseif($this->session->userdata('islogin')=='2'){
			redirect('beranda/solusi');
		}else {
			$this->load->helper(array('form', 'html'));
			$provinsi = $this->M_wilayah->get_provinsi();
			
			$data_hal = array ("provinsi"=>$provinsi);
			$this->load->view('user_page/registrasi',$data_hal);
		}
	}
	
	public function list_kabupaten()
	{
        $id = $this->input->post('id_prov');
        $data = $this->M_wilayah->get_kabupaten($id);
        echo json_encode($data);
	}
	
	public function cek()
	{
		$nim = addslashes($this->input->post('nim'));
		$nama = addslashes(strtoupper($this->input->post('nama')));
		$alamat = addslashes(strtoupper($this->input->post('alamat')));
		$id_prov = addslashes($this->input->post('id_provinsi'));
		$id_kab = addslashes($this->input->post('id_kabupaten'));
		$jurusan = addslashes(strtoupper($this->input->post('jurusan')));
		$pass = addslashes(md5($this->input->post('password')));
		$date_now = date('Y-m-d H:i:s');
		
		if($nim and $nama and $pass and $jurusan and $pass){
			$cek_mhs = $this->M_form->get_cek_mhs($nim);
			
			if(!$cek_mhs){
				$save_mhs = $this->M_form->save_regis($nim, $nama, $pass, $alamat, $id_prov, $id_kab, $jurusan, $date_now);
				
				echo"<script type='text/javascript'>alert('Registrasi Sukses!!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."daftar';</script>";
			} else {
				echo"<script type='text/javascript'>alert('Registrasi Gagal, Data Sudah Digunakan!!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."formulir';</script>";
			}
		}else{
			echo"<script type='text/javascript'>alert('Lengkapi Data!!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."formulir';</script>";
			exit();
		}
		
	}
	
}
