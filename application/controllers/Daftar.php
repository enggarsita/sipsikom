<?php
defined('BASEPATH') OR exit('No direct script access allowed');
ini_set('date.timezone', 'Asia/Jakarta');

class Daftar extends CI_Controller {
	
	function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->model('M_form');
	}
	
	public function index()
	{
		$islogin		= $this->session->userdata('islogin');
		
		if($this->session->userdata('islogin')=='1'){
			redirect('home');
		} elseif($this->session->userdata('islogin')=='2'){
			redirect('beranda/solusi');
		} else {
			$this->load->helper(array('form', 'html'));
			$this->load->view('user_page/login');
		}
	}
	
	public function cek()
	{
		$nim = addslashes($this->input->post('nim'));
		$pass = addslashes(md5($this->input->post('password')));
		
		if ($nim and $pass) {
			$cek_credential = $this->M_form->get_mhs($nim, $pass);
			
			if($cek_credential){
				$this->session->set_userdata('nim',$cek_credential[0]->nim);
				
				$this->session->set_userdata('islogin','2');
				
				redirect("beranda");
			} else{
				echo"<script type='text/javascript'>alert('NIM / Password Salah!!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."daftar';</script>";
				exit();
			}
		} else {
			redirect('admin_page/daftar?err=nopass');
		}
	}
	
	public function logout()
	{
		unset($_SESSION["username"]);
		unset($_SESSION["islogin"]);
		session_unset();
		session_destroy();
		echo"<script type='text/javascript'>window.location='".base_url()."daftar';</script>";
		
	}
	
	public function forgot()
	{
		$this->load->helper(array('form', 'html'));
		$this->load->view('user_page/forgot');
	}
	
	public function cek_forgot()
	{
		$nim = addslashes($this->input->post('nim'));
		$nama = addslashes(strtoupper($this->input->post('nama')));
		
		if ($nim and $nama) {
			$cek_data = $this->M_form->get_cek_user($nim, $nama);
			
			if($cek_data){
				$data['nim'] = $cek_data[0]->nim;
				$data['nama'] = $cek_data[0]->nama;
				
				$this->load->view('user_page/reset', $data);
				
			} else{
				echo"<script type='text/javascript'>alert('NIM / Nama Salah!!!!')</script>";
				echo"<script type='text/javascript'>window.location='".base_url()."daftar/forgot';</script>";
				exit();
			}
			
		} else {
			echo"<script type='text/javascript'>alert('Lengkapi Data!!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."daftar/forgot';</script>";
			exit();
		}
	}
	
	public function ganti_pass()
	{
		$nim = addslashes($this->input->post('nim'));
		$nama = addslashes(strtoupper($this->input->post('nama')));
		$pass = addslashes(md5($this->input->post('password')));
		$date_now = date('Y-m-d H:i:s');
		
		if ($nim and $nama and $pass) {
			$reset_pass = $this->M_form->save_pass($nim, $nama, $pass, $date_now);
			
			echo"<script type='text/javascript'>alert('Password Berhasil di Perbarui!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."daftar';</script>";
		} else {
			echo"<script type='text/javascript'>alert('Lengkapi Data!!!!')</script>";
			echo"<script type='text/javascript'>window.location='".base_url()."daftar/forgot';</script>";
			exit();
		}
	}
	
}
