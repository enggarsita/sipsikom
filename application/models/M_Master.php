<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_Master extends CI_Model
{
	public function get_user($user, $pass)
    {
		$where = "username='$user' AND password='$pass'";

		$this->db->from('m_admin');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
    }
	
	public function get_cek_user_regis($user, $no_hp)
	{
		$where = "username='$user' OR no_hp='$no_hp'";

		$this->db->from('m_admin');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
	}
	
	public function get_cek_user($user, $no_hp)
	{
		$where = "username='$user' AND no_hp='$no_hp'";

		$this->db->from('m_admin');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
	}
	
	public function save_pass($user, $no_hp, $pass, $date_now)
	{
		$sql = "UPDATE `m_admin` SET `password`='$pass', `update_date`='$date_now' WHERE `username`='$user' and `no_hp`='$no_hp'";
		$query = $this->db->query($sql);
	}
	
	public function save_regis($nama, $no_hp, $user, $pass, $date_now)
	{
		$sql = "INSERT INTO `m_admin`(`username`, `password`, `nama`, `no_hp`, `created_date`) VALUES ('$user','$pass','$nama','$no_hp','$date_now')";
		$query = $this->db->query($sql);
	}
	
	public function count_result()
	{
		$sql	= "SELECT nim FROM `m_jawaban` WHERE validasi='1' GROUP BY nim";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function count_verifikasi()
	{
		$sql	= "SELECT nim FROM `m_jawaban` WHERE validasi='0' GROUP BY nim";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function count_mahasiswa()
	{
		$sql	= "SELECT nim FROM `m_mahasiswa` WHERE nim!='0'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function count_konseling()
	{
		$sql	= "SELECT id FROM `m_admin`";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function count_kamus()
	{
		$sql	= "SELECT id FROM `m_kamus`";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function get_list_kamus()
	{
		$sql =  "SELECT * FROM `m_kamus`";
		$query = $this->db->query($sql);
        $data = $query->result();

        $data_list = array();

        $i = 1;
        foreach ($data as $dt) {
            $data_list[$i] = array("id" => $dt->id,"nm_kamus" => $dt->nm_kamus);
            $i++;
        }

        return $data_list;
	}
	
	public function add_kamus($judul, $isi, $date_now)
	{
		$sql = "INSERT INTO `m_kamus`(`nm_kamus`, `isi_kamus`, `create_date`) VALUES ('$judul', '$isi', '$date_now')";
		$query = $this->db->query($sql);
	}
	
	public function search_kamus($id)
	{
		$sql	= "SELECT * FROM `m_kamus` WHERE id='$id'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function edit_kamus($id, $judul, $isi, $date_now)
	{
		$sql = "UPDATE `m_kamus` SET `nm_kamus`='$judul',`isi_kamus`='$isi', `update_date`='$date_now' WHERE `id`='$id'";
		$query = $this->db->query($sql);
	}
	
	public function delete_kamus($id)
	{
		$sql = "DELETE FROM `m_kamus` WHERE id='$id'";
		$query = $this->db->query($sql);
	}
	
	public function get_list_result()
	{
		$sql =  "SELECT t1.nim, t2.nama, t1.created_date, t1.date_validasi, t2.jurusan FROM `m_jawaban` t1 LEFT JOIN `m_mahasiswa` t2 ON t1.nim = t2.nim WHERE t1.validasi='1' GROUP BY t1.nim";
		$query = $this->db->query($sql);
        $data = $query->result();

        $data_list = array();

        $i = 1;
        foreach ($data as $dt) {
            $data_list[$i] = array("nim" => $dt->nim, "nama" => $dt->nama, "created_date" => $dt->created_date,"date_validasi" => $dt->date_validasi,"jurusan" => $dt->jurusan);
            $i++;
        }

        return $data_list;
	}
	
	public function get_list_verifikasi()
	{
		$sql =  "SELECT t1.nim, t1.validasi, t2.nama, t1.created_date, t2.jurusan FROM `m_jawaban` t1 LEFT JOIN `m_mahasiswa` t2 ON t1.nim = t2.nim WHERE t1.validasi='0' GROUP BY t1.nim";
		$query = $this->db->query($sql);
        $data = $query->result();

        $data_list = array();

        $i = 1;
        foreach ($data as $dt) {
            $data_list[$i] = array("nim" => $dt->nim, "validasi" => $dt->validasi, "nama" => $dt->nama, "created_date" => $dt->created_date,"jurusan" => $dt->jurusan);
            $i++;
        }

        return $data_list;
	}
	
	public function search_data_verif_esay($nim)
	{
		$sql	= "SELECT * FROM `m_jawaban_esay` WHERE nim='$nim'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function search_data_verif($nim)
	{
		$sql	= "SELECT t1.*, t2.* FROM `m_jawaban` t1 LEFT JOIN `m_pertanyaan` t2 ON t1.id_soal = t2.id WHERE t1.nim='$nim'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function edit_verifikasi($nim, $date_now)
	{
		$sql = "UPDATE `m_jawaban` SET `validasi`='1',`date_validasi`='$date_now' WHERE `nim`='$nim'";
		$query = $this->db->query($sql);
	}
	
	public function edit_verifikasi_esay($nim, $date_now)
	{
		$sql = "UPDATE `m_jawaban_esay` SET `validasi`='1',`date_validasi`='$date_now' WHERE `nim`='$nim'";
		$query = $this->db->query($sql);
	}
	
	public function search_del_verif($nim)
	{
		$sql = "SELECT * FROM `m_jawaban` WHERE nim='$nim'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function search_del_verif_esay($nim)
	{
		$sql = "SELECT * FROM `m_jawaban_esay` WHERE nim='$nim'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function delete_verif($nim)
	{
		$sql = "DELETE FROM `m_jawaban` WHERE nim='$nim'";
		$query = $this->db->query($sql);
	}
	
	public function delete_verif_esay($nim)
	{
		$sql = "DELETE FROM `m_jawaban_esay` WHERE nim='$nim'";
		$query = $this->db->query($sql);
	}
	
	public function search_del_finish($nim)
	{
		$sql = "DELETE FROM `m_jawaban_finish` WHERE nim='$nim'";
		$query = $this->db->query($sql);
	}
	
	public function save_jawaban_end($nim, $jenis, $jml_solusi, $date_now)
	{
		$sql = "INSERT INTO `m_jawaban_finish`(`nim`, `jenis`, `skor`, `date_validasi`) VALUES ('$nim', '$jenis', '$jml_solusi', '$date_now')";
		$query = $this->db->query($sql);
	}
	
	public function get_pdf($nim)
	{
		$sql =  "SELECT t1.nim, t1.jenis, t2.nama FROM `m_jawaban_finish` t1 LEFT JOIN `m_mahasiswa` t2 ON t1.nim = t2.nim WHERE t1.nim='$nim'";
		$query = $this->db->query($sql);
        $data = $query->result();

        $data_list = array();

        $i = 1;
        foreach ($data as $dt) {
            $data_list[$i] = array("nama" => $dt->nama, "nim" => $dt->nim, "jenis" => $dt->jenis);
            $i++;
        }
		
        return $data_list;
	}	
}
?>