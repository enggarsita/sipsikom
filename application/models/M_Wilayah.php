<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_Wilayah extends CI_Model
{
	public function get_kabupaten($id)
    {
		$sql = "SELECT * FROM m_kabupaten WHERE id_kabupaten LIKE '$id%' ORDER BY nm_kabupaten ASC";
		$query  = $this->db->query($sql);
		return $query->result();
    }
	
	public function get_provinsi()
    {
		$sql = " SELECT * FROM m_provinsi ORDER BY nm_provinsi ASC";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		$aa=array();
		
		$i=1;
		foreach($data as $dt){
			$aa[$i]=array("nm_provinsi"=>$dt->nm_provinsi, "id_provinsi"=>$dt->id_provinsi);
			$i++;
		}
		return $aa;
    }
	
	public function get_idprovinsi($id_provinsi)
	{
		$where = "id_provinsi='$id_provinsi'";

		$this->db->from('m_provinsi');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
	}
	
	public function get_idkabupaten($id_kabupaten)
	{
		$where = "id_kabupaten='$id_kabupaten'";

		$this->db->from('m_kabupaten');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
	}
	
}

?>