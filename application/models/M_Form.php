<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_Form extends CI_Model
{
	public function get_mhs($nim, $pass)
    {
		$where = "nim='$nim' and password='$pass'";

		$this->db->from('m_mahasiswa');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
    }
	
	public function get_data_mhs($nim)
    {
		$where = "nim='$nim'";

		$this->db->from('m_mahasiswa');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
    }
	
	public function get_cek_user($nim, $nama)
	{
		$where = "nim='$nim' and nama='$nama'";

		$this->db->from('m_mahasiswa');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
	}
	
	public function save_pass($nim, $nama, $pass, $date_now)
	{
		$sql = "UPDATE `m_mahasiswa` SET `password`='$pass', `update_date`='$date_now' WHERE `nim`='$nim' and `nama`='$nama'";
		$query = $this->db->query($sql);
	}
	
	public function get_cek_mhs($nim)
	{
		$where = "nim='$nim'";

		$this->db->from('m_mahasiswa');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
	}
	
	public function save_regis($nim, $nama, $pass, $alamat, $id_prov, $id_kab, $jurusan, $date_now)
	{
		$sql = "INSERT INTO `m_mahasiswa`(`nim`, `nama`, `password`, `alamat`, `id_provinsi`, `id_kabupaten`, `jurusan`, `create_date`) VALUES ('$nim', '$nama', '$pass', '$alamat', '$id_prov', '$id_kab', '$jurusan', '$date_now')";
		$query = $this->db->query($sql);
	}
	
	public function get_jwb_esay_mhs($nim)
	{
		$where = "nim='$nim'";

		$this->db->from('m_jawaban_esay');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
	}
	
	public function save_jawaban_esay($nim, $keterangan, $date_now)
	{
		$sql = "INSERT INTO `m_jawaban_esay`(`nim`, `keterangan`, `created_date`, `validasi`) VALUES ('$nim', '$keterangan', '$date_now', '0')";
		$query = $this->db->query($sql);
	}
	
	public function get_jwb_mhs($nim)
	{
		$where = "nim='$nim'";

		$this->db->from('m_jawaban');
		$this->db->where($where);
		
		//print $this->db->last_query();
		return $this->db->get()->result();
	}
	
	public function get_list_pertanyaan()
	{
		$sql =  "SELECT * FROM `m_pertanyaan`";
		$query = $this->db->query($sql);
        $data = $query->result();

        $data_list = array();

        $i = 1;
        foreach ($data as $dt) {
            $data_list[$i] = array("id" => $dt->id,"pertanyaan" => $dt->pertanyaan,"skor" => $dt->skor);
            $i++;
        }

        return $data_list;
	}
	
	public function save_jawaban($nim, $id_no, $radio, $date_now)
	{
		$sql = "INSERT INTO `m_jawaban`(`nim`, `id_soal`, `jawaban`, `created_date`, `validasi`) VALUES ('$nim', '$id_no', '$radio', '$date_now', '0')";
		$query = $this->db->query($sql);
	}
	
	public function jumlah_skor($nim)
	{
		$sql = "SELECT SUM(jawaban) as skor FROM `m_jawaban` WHERE nim='$nim'";
		$query = $this->db->query($sql);
        $data = $query->result();
		
		return $data;
	}
	
	public function solusi($id)
	{
		$sql = "SELECT * FROM `m_solusi` WHERE id='$id'";
		$query = $this->db->query($sql);
        $data = $query->result();
		
		return $data;
	}
}

?>