<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_Menu extends CI_Model
{
	public function get_menu()
    {
		$where = "parent_menu='0' AND status_menu='1'";

		$this->db->from('m_menu');
		$this->db->where($where);
		$this->db->order_by('order_menu', 'ASC');
		
		//print $this->db->last_query();
		return $this->db->get()->result();
    }
	
	public function get_submenu($id_menu)
    {
		$where = "parent_menu='$id_menu' AND status_menu='1'";

		$this->db->from('m_menu');
		$this->db->where($where);
		$this->db->order_by('order_menu', 'ASC');
		
		//print $this->db->last_query();
		return $this->db->get()->result();
    }
	
}

?>