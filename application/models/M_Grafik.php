<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class M_Grafik extends CI_Model
{
	public function get_nilai1()
    {
		$sql = "SELECT COUNT(nim) as jumlah FROM `m_jawaban_finish` WHERE jenis LIKE 'Normal'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function get_nilai2()
    {
		$sql = "SELECT COUNT(nim) as jumlah FROM `m_jawaban_finish` WHERE jenis LIKE 'Depresi'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function get_nilai3()
    {
		$sql = "SELECT COUNT(nim) as jumlah FROM `m_jawaban_finish` WHERE jenis LIKE 'Depresi Psikotik'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function get_nilai4()
    {
		$sql = "SELECT COUNT(nim) as jumlah FROM `m_jawaban_finish` WHERE jenis LIKE 'Depresi Dysthymia'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function get_nilai5()
    {
		$sql = "SELECT COUNT(nim) as jumlah FROM `m_jawaban_finish` WHERE jenis LIKE 'Depresi Atypical'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}
	
	public function get_nilai6()
    {
		$sql = "SELECT COUNT(nim) as jumlah FROM `m_jawaban_finish` WHERE jenis LIKE 'Depresi Mayor'";
		$query  = $this->db->query($sql);
		$data   = $query->result();
		return $data;
	}

}

?>